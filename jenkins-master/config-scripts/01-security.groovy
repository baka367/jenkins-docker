#!groovy
import java.util.logging.Logger
import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule
import groovy.json.JsonSlurper

def instance = Jenkins.getInstance()
Logger logger = Logger.getLogger("")

logger.info('Started security config.')

String paramPath = System.getenv("JENKINS_PARAM_PATH")
try {
    paramText = new File("${paramPath}/security-params.json").text
} catch (FileNotFoundException e) {
    logger.severe("Cannot find param file path @ ${paramPath}/security-params.json")
    jenkins.doSafeExit(null)
    System.exit(1)
}

def list = new JsonSlurper().parseText( paramText )

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount(list.user, list.pass)
instance.setSecurityRealm(hudsonRealm)
 
def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()
 
Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)

logger.info('Finished security config.')