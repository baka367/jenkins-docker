#!groovy
/*
	Script to configure the slack plugin using parameter file slack-parameters.json
*/
import java.util.logging.Logger
import jenkins.model.*
import net.sf.json.JSONObject
import groovy.json.JsonSlurper

Logger logger = Logger.getLogger("")

JSONObject formData = ['slack': ['tokenCredentialId': '']] as JSONObject
def instance = Jenkins.getInstance()
def slack = instance.getExtensionList(
  jenkins.plugins.slack.SlackNotifier.DescriptorImpl.class
)[0]

String paramPath = System.getenv("JENKINS_PARAM_PATH")
try {
    paramText = new File("${paramPath}/slack-params.json").text
} catch (FileNotFoundException e) {
    logger.severe("Cannot find param file path @ ${paramPath}/slack-params.json")
    jenkins.doSafeExit(null)
    System.exit(1)
}

def params = new JsonSlurper().parseText( paramText )

def req = [
  getParameter: { name -> params[name] }
] as org.kohsuke.stapler.StaplerRequest

slack.configure(req, formData)
slack.save()
logger.info('Slack global settings configured.')