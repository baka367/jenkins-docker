#!groovy
/*
    Script to configure the github plugin using parameter file github-parameters.json
*/
import java.util.logging.Logger

import jenkins.*
import jenkins.model.*

import org.jenkinsci.plugins.github.config.GitHubPluginConfig
import org.jenkinsci.plugins.github.config.GitHubServerConfig
import org.jenkinsci.plugins.github.config.HookSecretConfig

import hudson.util.Secret
import com.cloudbees.plugins.credentials.domains.Domain
import org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl
import com.cloudbees.plugins.credentials.*;
import groovy.json.JsonSlurper

Logger logger = Logger.getLogger("")
Jenkins jenkins = Jenkins.getInstance()

def createCredentials(String credentialsId, String credentialsName, String credentials) {
    StringCredentialsImpl githubSecretText = new StringCredentialsImpl(
        CredentialsScope.GLOBAL,
        credentialsId,
        credentialsName,
        Secret.fromString(credentials)
    )
    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), githubSecretText)
}

String paramPath = System.getenv("JENKINS_PARAM_PATH")
try {
    paramText = new File("${paramPath}/github-params.json").text
} catch (FileNotFoundException e) {
    logger.severe("Cannot find param file path @ ${paramPath}/github-params.json")
    jenkins.doSafeExit(null)
    System.exit(1)
}

logger.info('Starting github plugin config')
def params = new JsonSlurper().parseText( paramText )

domain = Domain.global()
store = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()

List servers = []

params.servers.each { param ->
    logger.info('Creating credentials...')    
    createCredentials(param.credentials_id, "Github secret", param.credentials)

    GitHubServerConfig server = new GitHubServerConfig(param.credentials_id)
    server.name = param.name
    server.apiUrl = param.api_url
    server.manageHooks = param.manage_hooks
    server.clientCacheSize = param.client_cache_size

    servers << server
}


def global_settings = Jenkins.instance.getExtensionList(GitHubPluginConfig.class)[0]

if (params.hook_url != null) {
    global_settings.overrideHookUrl = true
    //Must start with protocol (i.e. http://)
    global_settings.hookUrl = new URL(params.hook_url)
}

if (params.shared_secret.credentials != null) {
    // createCredentials(params.shared_secret.credentials_id, "Github shared secret", params.shared_secret.credentials)
    StringCredentialsImpl githubSharedSecretText = new StringCredentialsImpl(
        CredentialsScope.GLOBAL,
        params.shared_secret.credentials_id,
        "Github shared secret",
        Secret.fromString(params.shared_secret.credentials)
    )
    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), githubSharedSecretText)
    global_settings.hookSecretConfig = new HookSecretConfig(params.shared_secret.credentials_id)
}

global_settings.setConfigs(servers)
global_settings.save()
// if (params.)
// global_settings.hookSecretConfig = new HookSecretConfig(github_plugin.optString('hookSharedSecretId'))