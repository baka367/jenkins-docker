// Adds a pipeline job to jenkins
import jenkins.model.Jenkins
import org.jenkinsci.plugins.workflow.job.WorkflowJob
import org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition
import org.jenkinsci.plugins.workflow.flow.FlowDefinition
import hudson.plugins.git.GitSCM
import hudson.plugins.git.UserRemoteConfig
import com.cloudbees.hudson.plugins.folder.*
import groovy.json.JsonSlurper

String paramPath = System.getenv("JENKINS_PARAM_PATH")
try {
    paramText = new File("${paramPath}/pipeline-params.json").text
} catch (FileNotFoundException e) {
    logger.severe("Cannot find param file path @ ${paramPath}/pipeline-params.json")
    jenkins.doSafeExit(null)
    System.exit(1)
}
def params = new JsonSlurper().parseText( paramText )


Jenkins jenkins = Jenkins.instance // saves some typing

// Get the folder where this job should be
def folder = jenkins.getItem(params.folder_name)
// Create the folder if it doesn't exist
if (folder == null) {
  folder = jenkins.createProject(Folder.class, params.folder_name)
}

// Create the git configuration
UserRemoteConfig userRemoteConfig = new UserRemoteConfig(params.git_repo, params.git_repo_name, null, params.credentials_id)

branches = null
doGenerateSubmoduleConfigurations = false
submoduleCfg = null
browser = null
gitTool = null
extensions = []
GitSCM scm = new GitSCM([userRemoteConfig], branches, doGenerateSubmoduleConfigurations, submoduleCfg, browser, gitTool, extensions)

// Create the workflow
FlowDefinition flowDefinition = (FlowDefinition) new CpsScmFlowDefinition(scm, params.job_script)

// Check if the job already exists
Object job = null
job = folder.getItem(params.job_name)
if (job == null) {
  oldJob = jenkins.getItem(params.job_name)
  if (oldJob.getClass() == WorkflowJob.class) {
    // Move any existing job into the folder
    Items.move(oldJob, folder)
  } else {
    // Create it if it doesn't
    job = folder.createProject(WorkflowJob, params.job_name)
  }
}
// Add the workflow to the job
job.setDefinition(flowDefinition)

// Set the branch somehow
job.save()