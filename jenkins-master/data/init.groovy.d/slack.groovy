/*
   Author: Glen Mailer (https://github.com/glenjamin)
           Sam Gleske (https://github.com/samrocketman)
   Original Script:
     https://github.com/jenkinsci/slack-plugin/issues/23
 */

import jenkins.model.*
import net.sf.json.JSONObject

JSONObject formData = ['slack': ['tokenCredentialId': '']] as JSONObject
def instance = Jenkins.getInstance()
def slack = instance.getExtensionList(
  jenkins.plugins.slack.SlackNotifier.DescriptorImpl.class
)[0]
//valid tokens for testing in the jenkins-slack-plugin-test instance of slack.com
def params = [
  slackBaseUrl: 'https://hooks.slack.com/services/TAS7GH03F/BAT09URUN/',
  slackTeamDomain: 'jenkins-slack-plugin',
  slackToken: 'peJTyPxHphUMJ692T7g83DL7',
  slackRoom: '#general',
  slackBuildServerUrl: 'http://localhost:8080/',
  slackSendAs: ''
]
def req = [
  getParameter: { name -> params[name] }
] as org.kohsuke.stapler.StaplerRequest
slack.configure(req, formData)

slack.save()
println 'Slack global settings configured.'