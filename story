get jenkins from jenkins/jenkins:lts

jenkins needs data partition.




Preinstalling plugins
You can rely on the install-plugins.sh script to pass a set of plugins to download with their dependencies. This script will perform downloads from update centers, and internet access is required for the default update centers.
Furthermore it is possible to pass a file that contains this set of plugins (with or without line breaks).

FROM jenkins/jenkins:lts
COPY jenkins-plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt



blog describing how to setup jenkins user and password
https://technologyconversations.com/2017/06/16/automating-jenkins-docker-setup/



automatic configuration for the slack plugin
SLACK PLUGIN CONFIG: https://github.com/jenkinsci/slack-plugin/issues/23



data partition setup:
https://engineering.riotgames.com/news/putting-jenkins-docker-container



python test setup:
https://qxf2.com/blog/jenkins-python/



react test setup:
https://blog.torq-dev.de/en/continuous-integration-on-jenkins-for-react-apps-scaffolded-with-create-react-app/


adding credentials to jenkins
https://support.cloudbees.com/hc/en-us/articles/217708168-create-credentials-from-groovy


Add bitbucket oauth to jenkins
http://codegists.com/code/bitbucket-oauth-plugin-jenkins/


create jenkins job through groovy


jenkins job on github
https://dzone.com/articles/how-to-start-working-with-the-github-plugin-for-je


jenkins pipeline 
https://wilsonmar.github.io/jenkins2-pipeline/


python with pipeline
https://jenkins.io/doc/tutorials/build-a-python-app-with-pyinstaller/
https://jenkins.io/doc/book/pipeline/docker/


jenkinsfile configs
https://jenkins.io/doc/book/pipeline/jenkinsfile/
